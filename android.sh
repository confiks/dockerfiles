#!/usr/bin/env bash
set -euxo pipefail

wget -q -O sdk.zip https://dl.google.com/android/repository/commandlinetools-linux-6858069_latest.zip
echo "87f6dcf41d4e642e37ba03cb2e387a542aa0bd73cb689a9e7152aad40a6e7a08 sdk.zip" | sha256sum -c

unzip -q sdk.zip -d "$ANDROID_HOME"
rm sdk.zip

# Packages
set +o pipefail
yes | sdkmanager --sdk_root="$ANDROID_HOME" --licenses
set -o pipefail

sdkmanager --sdk_root="$ANDROID_HOME" \
  "ndk;21.3.6528147" \
  "cmake;3.10.2.4988404" \
  "platforms;android-28" \
  "build-tools;28.0.3" \
  >/dev/null
